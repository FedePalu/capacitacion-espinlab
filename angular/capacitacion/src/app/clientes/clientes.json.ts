import {Cliente} from "./cliente"

export const CLIENTES: Cliente[] = [
 {id: 1, nombre: 'Andres', apellido: 'Gomez', createAt: '2019-03-05', email: 'agomez@gmail.com'},
 {id: 2, nombre: 'Pablo', apellido: 'Garcia', createAt: '2020-05-22', email: 'pgarcia@gmail.com'},
 {id: 3, nombre: 'Agustin', apellido: 'Diaz', createAt: '2018-11-15', email: 'adiaz@gmail.com'},
 {id: 4, nombre: 'Roman', apellido: 'Perez', createAt: '2013-12-03', email: 'rperez@gmail.com'},
 {id: 5, nombre: 'Sergio', apellido: 'Gonzales', createAt: '2017-06-06', email: 'sgonzales@gmail.com'}
];
