import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Capacitacion';
  curso : string = 'Este es el cursito';
  alumno : string = 'Alumno: Federico Palumbo';
}
